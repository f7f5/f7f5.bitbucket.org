var DEBUG = (window.location.search.indexOf("debug") != -1);
var ARENA_WIDTH=800, ARENA_HEIGHT=480; // TODO use viewport
var fps = Crafty.timer.getFPS();
var wave, trace;
var gameReady=true;
var current_level="Level1";
var level = 1;

var TEXT_CSS = {
    "font-family": "Arial",
    "font-size": "32px",
    "text-align": "center",
    "font-weight": "bold",
    "color": "green"
};

function next_level() {
    if (level == 0) return;
    if (++level > 6) {
        Crafty.scene("WellDone");
    } else {
        current_level = "Level" + level.toString();
        Crafty.scene(current_level);
    }
}

// sin(z), z can be: {steps, t, x}
Crafty.c("sinewave", {
    _amplitude: 150,
    _frequency: 1,
    init: function() {
        this.bind("EnterFrame", function() {
            if (!gameReady) return;
            var alpha = 2 * Math.PI * this._frequency * (Crafty.frame()- this._frame0) / fps;
            this.y = this._amplitude * Math.sin(alpha) + 200;
        });
    },
    sinewave: function() {
        this._frame0 = Crafty.frame();
    }
});


// sin(z), z can be: {steps, t, x}
Crafty.c("MovingSinewave", {
    _max_amplitude: 200,
    _amplitude: 150,
    _target_amplitude: null,
    _max_frequency: 2,
    _target_frequency: null,
    _frequency: 1,
    _xspeed: 3,
    _der: 0,
    init: function() {
        this.bind("EnterFrame", function() {
            if (!gameReady) return;
            if (this._frame0 === null) {
                this._frame0 = Crafty.frame();
            }
            var alpha = 2 * Math.PI * this._frequency * (Crafty.frame()- this._frame0) / fps;
            var old_y = this.y;
            this.y = this._amplitude * Math.sin(alpha) + this.centre;
            if (this._target_frequency !== null) {
                if ((this.y == 0) || ((old_y - this.centre) * (this.y - this.centre) < 0)) {
                    this._frequency = this._target_frequency;
                    this._target_frequency = null;
                    this._frame0 = Crafty.frame() - alpha/(2 * Math.PI * this._frequency) * fps;
                    
                }
            }
            if (this._target_amplitude !== null) {
                if ((this.y == 0) || ((old_y - this.centre) * (this.y - this.centre) < 0)) {
                    this._amplitude = this._target_amplitude;
                    this._target_amplitude = null;
                }
            }
            //Sounds
            var der = this.y - old_y;
            if ((der * this._der) <= 0) {
                if ((der > 0) || (this._der < 0)) {
                    Crafty.audio.play("blip");
                } else {
                    Crafty.audio.play("blop");
                }
            }
            this._der = der;
            
            this.x += this._xspeed;
            if (this.x > ARENA_WIDTH) {
                //this.x -= 800;
                next_level();
//                 Crafty.pause();
            }
            trace.addPoint(this.x, this.y);
        });
    },
    set_amplitude: function(A) {
        if (A > this._max_amplitude) {
            A = this._max_amplitude;
        } else if (A < 5) {
            A = 5;
        }
        this._target_amplitude = A;
    },
    set_frequency: function(f) {
        if (f > this._max_frequency) {
            f = this._max_frequency;
        } else if (f < 0.1) {
            f = 0.1;
        }
        this._target_frequency = f;
    },
    movingsinewave: function(centre) {
        this.centre = centre;
        this._frame0 = null;
    }
});


Crafty.c("AmpPresetKeyboardControl", {
    _levels: [0.1, 0.25, 0.5, 0.75, 1],
    init: function() {
        this.bind("KeyDown", function(e) {
            var q=undefined;
            switch(e.key) {
                case Crafty.keys['1']:
                    q = 0;
                    break;
                case Crafty.keys['2']:
                    q = 1;
                    break;
                case Crafty.keys['3']:
                    q = 2;
                    break;
                case Crafty.keys['4']:
                    q = 3;
                    break;
                case Crafty.keys['5']:
                    q = 4;
                    break;
            };
            if (q != undefined) {
                this.set_amplitude(this._levels[q] * this._max_amplitude);
            }
        });
    }
});


Crafty.c("FreqPresetKeyboardControl", {
    _levels: [0.1, 0.25, 0.5, 0.75, 1],
    init: function() {
        this.bind("KeyDown", function(e) {
            var q=undefined;
            switch(e.key) {
                case Crafty.keys['6']:
                    q = 0;
                    break;
                case Crafty.keys['7']:
                    q = 1;
                    break;
                case Crafty.keys['8']:
                    q = 2;
                    break;
                case Crafty.keys['9']:
                    q = 3;
                    break;
                case Crafty.keys['0']:
                    q = 4;
                    break;
            };
            if (q != undefined) {
                this.set_frequency(this._levels[q] * this._max_frequency);
            }
        });
    }
});


Crafty.c("AmpKeyboardControl", {
    init: function() {
        this.bind("KeyDown", function(e) {
            var q = undefined;
            switch(e.key) {
                case Crafty.keys['UP_ARROW']:
                    q = 1;
                    break;
                case Crafty.keys['DOWN_ARROW']:
                    q = -1;
                    break;
            }
            if (q != undefined) {
                this.set_amplitude(this._amplitude + q * 20);
            }
        });
    }
});


Crafty.c("Tracex", {
    _points: [
    ],
    init:  function() {
    },
    // http://louisstowasser.com/post/21479078234/custom-canvas-drawing-in-craftyjs
    draw: function() {
        if (this.has("Canvas")) {
            var ctx = Crafty.canvas.context;
//             ctx.save(); This is wrong in the post.
            ctx.fillStyle = "#000";
            for (var i=0; i<this._points.length; i++) {
                var p = this._points[i];
                ctx.fillRect(p.x, p.y, 1, 1);
            }
        } else {
            console.log('No DOM');
        }
    },
    addPoint: function(x, y) {
        this._points.push({x: x, y: y})
    },
    tracex: function() {
        this._points = [];
        return this;
    }
});

Crafty.c("CustomHTML", {
    init: function () {
        this.requires("2D, DOM");

        this.bind("Draw", function (e) {
            var el = this._element,
                style = el.style;

            el.innerHTML = this.source.innerHTML;
            el.style.background = 'rgba(0, 0, 0, 0.5)';
        });
    },
    customhtml: function(el) {
        this.source = el;
        return this;
    }
});

function createLevel(level) {
    var y;
    for (var i=0; i<level.length; ++i) {
        var el = level[i];
        if (el.type == 'Stalactite') {
            if (el.fix == 'top') {
                y = 0;
            } else if (el.fix == 'bottom') {
                y = Crafty.viewport.height - el.h;
            }
            Crafty.e("Stalactite, 2D, Canvas, Color").attr({ x: el.x, y: y, w: el.w, h: el.h }).color("#000");
        } else if (el.type == 'Buoy') {
            Crafty.e("2D, Canvas, Color, Buoy").attr({ x: el.x, y: el.y, w: el.w, h: el.h }).color("#000");
        }
    }
}

/* Obstacles */
Crafty.c("Stalactite", {});

Crafty.c("Buoy", {});


function die(hitData) {
    gameReady = false;
    Crafty.DrawManager.draw();
    Crafty.pause();
    
    Crafty.e("Death, DOM, 2D, Text")
        .attr({ x: 0, w: Crafty.viewport.width, h: 40, y: Crafty.viewport.height/2 - 20})
        .text("Bad luck!")
        .css(TEXT_CSS);
        
    setTimeout(function() {
            Crafty.scene("void");
            Crafty.scene(current_level);
//             gameReady = true;
            Crafty.pause();
        }, 3000 //ms
    );
}


/* Scenes */

// empty scene
Crafty.scene("void", function() {});

// Main game
Crafty.scene("Level1", function() {
    trace = Crafty.e("2D, Canvas, Tracex")
        .attr({x:0, y:0, w: ARENA_WIDTH, h: ARENA_HEIGHT})
        .tracex();
        
        
    /// Level 1
    var LEVEL_1 = [
        {type: "Stalactite", x: 170, w: 280, h: 200, fix: "bottom"},
        {type: "Stalactite", x: 170, w: 280, h: 200, fix: "top"},
        {type: "Buoy", x: 560, y: 70, w: 11, h: 340},
        {type: "Buoy", x: 709, y: 70, w: 11, h: 340}
    ];
    createLevel(LEVEL_1);
            
        
    wave = Crafty.e("2D, Canvas, Color, MovingSinewave, AmpPresetKeyboardControl, Collision")
            .attr({ x: 0, y: ARENA_HEIGHT/2, w: 5, h: 5})
            .color("#000")
            .collision() // Don't forget to call collision!!!!!
            .onHit('Stalactite', die)
            .onHit('Buoy', die)
            .movingsinewave(ARENA_HEIGHT/2);
            
    var get_ready = Crafty.e("2D, DOM, CustomHTML")
        .attr({ w: Crafty.viewport.width, h: Crafty.viewport.height})
        .customhtml(document.getElementById('level1-instructions'))
        .bind("KeyDown", function() {
            get_ready.destroy();
            gameReady = true;
            Crafty.audio.stop('tune1');
        });
    gameReady = false;
    Crafty.audio.play('tune1');
});

Crafty.scene("Level2", function() {
    trace = Crafty.e("2D, Canvas, Tracex")
        .attr({x:0, y:0, w: ARENA_WIDTH, h: ARENA_HEIGHT})
        .tracex();
        
        
    /// Level 1
    var LEVEL_2 = [
        {type: "Stalactite", x: 103, w: 20, h: ARENA_HEIGHT-122, fix: "bottom"},
        {type: "Stalactite", x: 165, w: 180, h: 210, fix: "top"},
        {type: "Stalactite", x: 165, w: 180, h: 210, fix: "bottom"},
        {type: "Stalactite", x: 475, w: 25, h: 300, fix: "top"},
        {type: "Stalactite", x: 350, w: 275, h: ARENA_HEIGHT-397, fix: "bottom"},
        {type: "Stalactite", x: 627, w: ARENA_WIDTH-627, h: ARENA_HEIGHT-360, fix: "bottom"},
        {type: "Stalactite", x: 775, w: 25, h: 279, fix: "top"},
        {type: "Buoy", x: 622, y: 197, w: 38, h: ARENA_HEIGHT-197*2}
    ]
    createLevel(LEVEL_2);
            
        
    wave = Crafty.e("2D, Canvas, Color, MovingSinewave, AmpPresetKeyboardControl, Collision")
            .attr({ x: 0, y: ARENA_HEIGHT/2, w: 5, h: 5})
            .color("#000")
            .collision() // Don't forget to call collision!!!!!
            .onHit('Stalactite', die)
            .onHit('Buoy', die)
            .movingsinewave(ARENA_HEIGHT/2);
            
    var get_ready = Crafty.e("2D, DOM, CustomHTML")
        .attr({ w: Crafty.viewport.width, h: Crafty.viewport.height})
        .customhtml(document.getElementById('level2-instructions'))
        .bind("KeyDown", function() {
            get_ready.destroy();
            gameReady = true;
            Crafty.audio.stop('tune1');
        });
    gameReady = false;
    Crafty.audio.play('tune1');

//     Crafty.pause();
});



// Main game
Crafty.scene("Level3", function() {
    trace = Crafty.e("2D, Canvas, Tracex")
        .attr({x:0, y:0, w: ARENA_WIDTH, h: ARENA_HEIGHT})
        .tracex();
        
    var LEVEL_3 = [
        {type: "Buoy", x: 615, y: 114, w: 7, h: 256},
        {type: "Buoy", x: 666, y: 114, w: 7, h: 256},
        {type: "Buoy", x: 713, y: 114, w: 7, h: 256},
        {type: "Stalactite", x: 265, w: 275, h: 283, fix: "bottom"}
    ]
    createLevel(LEVEL_3);
        
        
    wave = Crafty.e("2D, Canvas, Color, MovingSinewave, FreqPresetKeyboardControl, Collision")
            .attr({ x: 0, y: ARENA_HEIGHT/2, w: 5, h: 5})
            .color("#000")
            .collision() // Don't forget to call collision!!!!!
            .onHit('Stalactite', die)
            .onHit('Buoy', die)
            .movingsinewave(ARENA_HEIGHT/2);
            
    var get_ready = Crafty.e("2D, DOM, CustomHTML")
        .attr({ w: Crafty.viewport.width, h: Crafty.viewport.height})
        .customhtml(document.getElementById('level4-instructions'))
        .bind("KeyDown", function() {
            get_ready.destroy();
            gameReady = true;
            Crafty.audio.stop('tune1');
        });
    Crafty.audio.play('tune1');
    gameReady = false;
});

// Main game
Crafty.scene("Level4", function() {
    trace = Crafty.e("2D, Canvas, Tracex")
        .attr({x:0, y:0, w: ARENA_WIDTH, h: ARENA_HEIGHT})
        .tracex();
        
    /// Level 2
    /// This one's difficult: 0, 9 for the last buoy, 7
    var LEVEL_4 = [
        {type: "Buoy", x: 90, y: 210, w: 15, h: 60}, // FIXME no need for y
        {type: "Buoy", x: 125, y: 210, w: 15, h: 60}, 
        {type: "Buoy", x: 162, y: 210, w: 15, h: 60}, 
        {type: "Buoy", x: 201, y: 210, w: 15, h: 60}, 
        {type: "Buoy", x: 242, y: 210, w: 15, h: 60}, 
        
    
    
        {type: "Stalactite", x: 340, w: 115, h: ARENA_HEIGHT-230, fix: "bottom"},
        {type: "Stalactite", x: 470, w: 160, h: 200, fix: "top"},
        {type: "Stalactite", x: 620, w: 160, h: 200, fix: "bottom"}
        
    ]
    createLevel(LEVEL_4);
        
        
    wave = Crafty.e("2D, Canvas, Color, MovingSinewave, FreqPresetKeyboardControl, Collision")
            .attr({ x: 0, y: ARENA_HEIGHT/2, w: 5, h: 5})
            .color("#000")
            .collision() // Don't forget to call collision!!!!!
            .onHit('Stalactite', die)
            .onHit('Buoy', die)
            .movingsinewave(ARENA_HEIGHT/2);
            
    var get_ready = Crafty.e("2D, DOM, CustomHTML")
        .attr({ w: Crafty.viewport.width, h: Crafty.viewport.height})
        .customhtml(document.getElementById('level4-instructions'))
        .bind("KeyDown", function() {
            get_ready.destroy();
            gameReady = true;
            Crafty.audio.stop('tune1');
        });
    Crafty.audio.play('tune1');
    gameReady = false;
});

Crafty.scene("Level5", function() {
    trace = Crafty.e("2D, Canvas, Tracex")
        .attr({x:0, y:0, w: ARENA_WIDTH, h: ARENA_HEIGHT})
        .tracex();
        
    /// Level 6 : 2, 9, 4+7
    var LEVEL_5 = [
        {type: "Buoy", x: 110, y: 58, w: 10, h: 365},
        {type: "Buoy", x: 186, y: 58, w: 10, h: 365},
        {type: "Buoy", x: 261, y: 58, w: 10, h: 365},
    
        {type: "Stalactite", x: 335, w: 280, h: 298, fix: "top"},
        {type: "Stalactite", x: 704, w: 96, h: 255, fix: "bottom"}
        
    ]
    createLevel(LEVEL_5);
        
        
    wave = Crafty.e("2D, Canvas, Color, MovingSinewave, FreqPresetKeyboardControl, AmpPresetKeyboardControl, Collision")
            .attr({ x: 0, y: ARENA_HEIGHT/2, w: 5, h: 5})
            .color("#000")
            .collision() // Don't forget to call collision!!!!!
            .onHit('Stalactite', die)
            .onHit('Buoy', die)
            .movingsinewave(ARENA_HEIGHT/2);
            
    var get_ready = Crafty.e("2D, DOM, CustomHTML")
        .attr({ w: Crafty.viewport.width, h: Crafty.viewport.height})
        .customhtml(document.getElementById('level5-instructions'))
        .bind("KeyDown", function() {
            get_ready.destroy();
            gameReady = true;
            Crafty.audio.stop('tune1');
        });
    Crafty.audio.play('tune1');
    gameReady = false;
});


// Main game
Crafty.scene("Level6", function() {
    trace = Crafty.e("2D, Canvas, Tracex")
        .attr({x:0, y:0, w: ARENA_WIDTH, h: ARENA_HEIGHT})
        .tracex();
        
    /// Level 6 : 2, 9, 4+7
    var LEVEL_6 = [
        {type: "Buoy", x: 172, y: 139, w: 210-172, h: 122},
        {type: "Buoy", x: 295, y: 139, w: 15, h: 122}, 
        {type: "Buoy", x: 580, y: 130, w: 42, h: 220}, 
        {type: "Buoy", x: 700, y: 195, w: 100, h: 90}, 
        
    
    
        {type: "Stalactite", x: 103, w: 378-103, h: ARENA_HEIGHT-315, fix: "bottom"},
        {type: "Stalactite", x: 395, w: 105, h: 263, fix: "top"},
        {type: "Stalactite", x: 550, w: 250, h: 80, fix: "top"}     
    ]
    createLevel(LEVEL_6);
        
        
    wave = Crafty.e("2D, Canvas, Color, MovingSinewave, FreqPresetKeyboardControl, AmpPresetKeyboardControl, Collision")
            .attr({ x: 0, y: ARENA_HEIGHT/2, w: 5, h: 5})
            .color("#000")
            .collision() // Don't forget to call collision!!!!!
            .onHit('Stalactite', die)
            .onHit('Buoy', die)
            .movingsinewave(ARENA_HEIGHT/2);
            
    var get_ready = Crafty.e("2D, DOM, CustomHTML")
        .attr({ w: Crafty.viewport.width, h: Crafty.viewport.height})
        .customhtml(document.getElementById('level6-instructions'))
        .bind("KeyDown", function() {
            get_ready.destroy();
            gameReady = true;
            Crafty.audio.stop('tune1');
        });
    Crafty.audio.play('tune1');
    gameReady = false;
});

// Game completed
Crafty.scene("WellDone", function() {
    Crafty.e("2D, DOM, Text")
        .attr({ w: Crafty.viewport.width, h: 40, y: Crafty.viewport.height/2 -40})
        .text("Congratulations!")
        .css(TEXT_CSS);
    Crafty.e("2D, DOM, Text")
        .attr({ w: Crafty.viewport.width, h: 20, y: Crafty.viewport.height/2 + 20})
        .text("See you on LD #27 ;)")
        .css(TEXT_CSS);
    Crafty.audio.play('tune2', -1);
});

// TEST
Crafty.scene("Level0", function() {
    trace = Crafty.e("2D, Canvas, Tracex")
        .attr({x:0, y:0, w: ARENA_WIDTH, h: ARENA_HEIGHT})
        .tracex();
        
    wave = Crafty.e("2D, Canvas, Color, MovingSinewave, FreqPresetKeyboardControl, AmpPresetKeyboardControl, Collision")
            .attr({ x: 0, y: ARENA_HEIGHT/2, w: 5, h: 5})
            .color("#000")
            .collision() // Don't forget to call collision!!!!!
            .onHit('Stalactite', die)
            .onHit('Buoy', die)
            .movingsinewave(ARENA_HEIGHT/2);
            
    var get_ready = Crafty.e("2D, DOM, CustomHTML")
        .attr({ w: Crafty.viewport.width, h: Crafty.viewport.height})
        .customhtml(document.getElementById('level2-instructions'))
        .bind("KeyDown", function() {
            get_ready.destroy();
            gameReady = true;
        });
    gameReady = false;
});


/// Initialization
window.onload = function () {
    if (!window.HTMLCanvasElement) {
        document.getElementById('nocanvas').style.display = 'block';
        return;
    }
    if (DEBUG) {
        document.getElementById('parameters').style.display = 'block';
    }
    init_parameters_ctrl();
    //start crafty
    Crafty.audio.add("tune1", [
        "tune1.wav",
    ]);
    Crafty.audio.add("tune2", [
        "tune2.wav",
    ]);
    Crafty.audio.add("blip", [
        "sounds/Blip1.wav",
    ]);
    Crafty.audio.add("blop", [
        "sounds/Blip2.wav",
    ]);
    
    Crafty.init(ARENA_WIDTH, ARENA_HEIGHT);
    Crafty.canvas.init();
    current_level = "Level1";
    Crafty.scene("Level1");
};



///// Parameters
function init_parameters_ctrl() {
    var pause_btn = document.getElementById('pausePr');
    pause_btn.onclick = function () {
        Crafty.pause();
    };
    
    var amplitude_inp = document.getElementById('amplitudePr');
    amplitude_inp.onchange = function () {
        wave._amplitude = parseInt(this.value, 10);
    };
    
    var frequency_btn = document.getElementById('frequencyPr');
    frequency_btn.onchange = function () {
        wave._frequency = parseFloat(this.value);
    };
    
    var xspeed_btn = document.getElementById('xspeedPr');
    xspeed_btn.onchange = function () {
        wave._xspeed = parseFloat(this.value);
    };
    
    var amplitude_slider = document.getElementById('amplitudeSlider');
    amplitude_slider.onchange = function () {
        wave.set_amplitude(parseInt(this.value, 10));
    };
}
