var DEBUG = (window.location.search.indexOf("debug") != -1);
var ARENA_WIDTH=800, ARENA_HEIGHT=700;
var ROOM_SIZE=600;
var DOOR_SIZE=50;
var WALL_SIZE=20;

/// Initialization
window.onload = function () {
    if (!window.HTMLCanvasElement) {
        document.getElementById('nocanvas').style.display = 'block';
        return;
    }

    // Assets
    Crafty.audio.add("tune", [
        "snd/tune.ogg",
    ]);
    Crafty.audio.add("gate", [
        "snd/gate.wav",
    ]);
    Crafty.audio.add("pickup", [
        "snd/pickup.wav",
    ]);
    Crafty.audio.add("switch", [
        "snd/switch.wav",
    ]);
    
    Crafty.init(ARENA_WIDTH, ARENA_HEIGHT);
    Crafty.canvas.init();
    Crafty.background('#000');
    Crafty.audio.play('tune', -1);
    document.getElementById('togMus').onclick = function() {
        Crafty.audio.togglePause('tune');
    };
    next_level();
//     Crafty.scene("Level1");
};


Crafty.c("room", {
});


Crafty.c("door", {
    _exit: null,
    door: function(dir) {
        this.exit = dir;
        switch(dir) {
            case 'W':
                this.attr({x: 0, y: ROOM_SIZE/2-(DOOR_SIZE/2), w: 20, h: DOOR_SIZE})
                break;
            case 'E':
                this.attr({x: ROOM_SIZE-20, y: ROOM_SIZE/2-(DOOR_SIZE/2), w: 20, h: DOOR_SIZE})
                break;
            case 'N':
                this.attr({x: ROOM_SIZE/2-(DOOR_SIZE/2), y: 0, w: DOOR_SIZE, h: 20})
                break;
            case 'S':
                this.attr({x: ROOM_SIZE/2-(DOOR_SIZE/2), y: ROOM_SIZE-20, w: DOOR_SIZE, h: 20})
                break;
                
        }
        return this;
    }
});


Crafty.c("switch", {
    colors: ['#F00', '#683f3f'],
    c: 0,
    swtx: function(pos, original) {
        this.rotate = pos;
        this.original = original;
        this.c = this.original.state ? this.original.state : 0
        this.color(this.colors[this.c]);
        return this;
    },
    toggle: function() {
        if ((++this.c) >= this.colors.length) {
            this.c = 0;
        }
        this.original.state = this.c;
        this.color(this.colors[this.c]);
    }
});


Crafty.c("gate", {
    _keys: null,
    gate: function(keyIds) {
        this._keys = keyIds;
        this.attr({x: 0, y: ROOM_SIZE/2-50, w: 20, h: 100})
        return this;
    },
    open: function(keys) {
        for (var i=0; i<this._keys.length; i++) {
          if (!this.find(this._keys[i], keys)) {
              return false;
          }
        }
        return true;
    },
    find: function(k, arr) {
        for (var i=0; i<arr.length; i++) {
            if (arr[i].id == k) {
                return true;
            }
        }
        return false;
    }
});


Crafty.c("CustomHTML", {
    init: function () {
        this.requires("2D, DOM");

        this.bind("Draw", function (e) {
            var el = this._element,
                style = el.style;

            el.innerHTML = this.source.innerHTML;
            el.style.background = 'rgba(0, 0, 0, 0.5)';
        });
    },
    customhtml: function(el) {
        this.source = el;
        return this;
    }
});


MAP1 = function() {
    this.rotateGrid = function(pos) {
        if (!(pos instanceof Array)) {
            pos = [pos]
        }
        for (var p=0; p<pos.length; p++) {
            var g=this.grid[pos[p]];
            g.ptr++;
            if (g.ptr >= g.rooms.length ) {
                g.ptr = 0;
            }
            g.currentRoom = g.rooms[g.ptr];
        }
    },
    this.move = function(dir) {
        var cell = this.grid[this.gridPos];
        this.gridPos = cell[dir]; // new grid pos
        cell = this.grid[this.gridPos];
        
    },
    this.pick = function(objId) {
        var room = this.rooms[this.grid[this.gridPos].currentRoom];
        for (i=0; i<room.objects.length; i++) {
            var o = room.objects[i];
            if (o.id == objId) {
                room.objects.pop(o);
                return o;
            }
        }
    },
    this.rooms = [
            { // 0
                doors: [{leads: 'E'}],
                objects: [],
                gate: {color: '#0F0', needs: ['greenKey']}
            },
            { // 1
                doors: [{leads: 'W'}],
                objects: [{x: 300, y:300, w: 30, h: 30, type: 'switch', rotate: 0}]
            },
            { // 2
                doors: [{leads: 'E'}],
                objects: [{x: 60, y:40, w: 25, h: 15, type: 'key', id: 'greenKey', color: '#0F0'}]
            }
    ],
    this.grid = [
        { 
            currentRoom: 0,
            rooms: [0, 2],
            ptr: 0,
            'E': 1,
        },
        { 
            currentRoom: 1,
            rooms: [1],
            ptr: 0,
            'W': 0
        }
    ],
    this.gridPos = 0
}


MAP2 = function() {
    this.rotateGrid = function(pos) {
        var g=this.grid[pos];
        g.ptr++;
        if (g.ptr >= g.rooms.length ) {
            g.ptr = 0;
        }
        g.currentRoom = g.rooms[g.ptr];
    },
    this.move = function(dir) {
        var cell = this.grid[this.gridPos];
        this.gridPos = cell[dir]; // new grid pos
        cell = this.grid[this.gridPos];
        
    },
    this.rooms = [
            { // 0
                doors: [{leads: 'E'}],
                objects: [],
                gate: {image: 'img/gate2.png', needs: ['greenKey', 'blueKey']}
            },
            { // 1
                doors: [{leads: 'W'}, {leads: 'E'}],
                objects: []
            },
            { // 2
                doors: [{leads: 'W'},],
                objects: [{x: 300, y:300, w: 30, h: 30, type: 'switch', rotate: 0}, {x: 300, y:200, w: 30, h: 30, type: 'switch', rotate: 1}]
            },
            { // 3
                doors: [{leads: 'E'}],
                objects: [{x: 50, y:50, w: 25, h: 15, type: 'key', id: 'greenKey', color: '#0F0'}]
            },
            { // 4
                doors: [{leads: 'E'}, {leads: 'N'}],
                objects: []
            },
            { // 5
                doors: [{leads: 'S'}],
                objects: [{x: 50, y:50, w: 25, h: 15, type: 'key', id: 'blueKey', color: '#00F'}]
            }
    ],
    this.grid = [
        { 
            currentRoom: 0,
            rooms: [0, 3],
            ptr: 0,
            'E': 1,
        },
        { 
            currentRoom: 1,
            rooms: [1, 4],
            ptr: 0,
            'E': 2,
            'W': 0,
            'N': 3,
        },
        { 
            currentRoom: 2,
            rooms: [2],
            ptr: 0,
            'W': 1,
        },
        { 
            currentRoom: 5,
            rooms: [5],
            ptr: 0,
            'S': 1,
        },
    ],
    this.gridPos = 0,
    this.pick = function(objId) {
        var room = this.rooms[this.grid[this.gridPos].currentRoom];
        for (i=0; i<room.objects.length; i++) {
            var o = room.objects[i];
            if (o.id == objId) {
                room.objects.pop(o);
                return o;
            }
        }
    }
}


var MAP3 = function() {
    this.rotateGrid = function(pos) {
        if (!(pos instanceof Array)) {
            pos = [pos]
        }
        for (var p=0; p<pos.length; p++) {
            var g=this.grid[pos[p]];
            g.ptr++;
            if (g.ptr >= g.rooms.length ) {
                g.ptr = 0;
            }
            g.currentRoom = g.rooms[g.ptr];
        }
    },
    this.move = function(dir) {
        var cell = this.grid[this.gridPos];
        this.gridPos = cell[dir]; // new grid pos
        cell = this.grid[this.gridPos];
        
    },
    this.rooms = [
            { // 0
                doors: [{leads: 'E'}],
                objects: [],
                gate: {image: 'img/gate2.png', needs: ['greenKey', 'blueKey']}
            },
            { // 1
                doors: [{leads: 'W'}, {leads: 'E'}],
                objects: []
            },
            { // 2
                doors: [{x: 0, y:210, w: 5, h: 30, leads: 'W'},],
                objects: [{x: 300, y:300, w: 30, h: 30, type: 'switch', rotate: 1}, {x: 300, y:200, w: 30, h: 30, type: 'switch', rotate: [3,0]}]
            },
            { // 3
                doors: [{leads: 'E'}, {leads: 'N'}],
                objects: []
            },
            { // 4
                doors: [{leads: 'S'}],
                objects: [{x: 50, y:50, w: 25, h: 15, type: 'key', id: 'greenKey', color: '#0F0'}]
            },
            { // 5
                doors: [{leads: 'E'}, {leads: 'S'}],
                objects: []
            },
            { // 6
                doors: [{leads: 'W'}],
                objects: [{x: 50, y:50, w: 25, h: 15, type: 'key', id: 'blueKey', color: '#00F'}]
            },
            { // 7
                doors: [{leads: 'E'}],
                objects: []
            }
    ],
    this.grid = [
        { 
            currentRoom: 0,
            rooms: [0, 7],
            ptr: 0,
            'E': 1,
        },
        { 
            currentRoom: 1,
            rooms: [1, 3],
            ptr: 0,
            'E': 2,
            'W': 0,
            'N': 3,
        },
        { 
            currentRoom: 2,
            rooms: [2],
            ptr: 0,
            'W': 1,
        },
        { 
            currentRoom: 4,
            rooms: [4, 5],
            ptr: 0,
            'S': 1,
            'E': 4
        },
        { 
            currentRoom: 6,
            rooms: [6],
            ptr: 0,
            'W': 3,
        },
    ],
    this.gridPos = 0,
    this.pick = function(objId) {
        var room = this.rooms[this.grid[this.gridPos].currentRoom];
        for (i=0; i<room.objects.length; i++) {
            var o = room.objects[i];
            if (o.id == objId) {
                room.objects.pop(o);
                return o;
            }
        }
    }
}


Crafty.c("hero", {
    doAction: false,
    kd: false,
    init: function() {
        this.pocket = new Array();
        this.bind("EnterFrame", function() {
            // WALL COLLISION
            if (this.x + this.w > (ROOM_SIZE - WALL_SIZE)) {
                this.x = ROOM_SIZE - WALL_SIZE - this.w;
            } else if (this.x < (0 + WALL_SIZE)) {
                this.x = 0 + WALL_SIZE;
            }
            if (this.y + this.h > (ROOM_SIZE - WALL_SIZE)) {
                this.y = ROOM_SIZE - WALL_SIZE - this.h;
            } else if (this.y < (0 + WALL_SIZE)) {
                this.y = 0 + WALL_SIZE;
            }
            
        });
    },
    pick: function(obj) {
        this.pocket.push(obj);
        globalPocket.show(obj);
    },
    canDoAction: function() {
        if (!this.kd) {
            this.doAction = true;
            this.kd= true;
        }
        return this.doAction;
    },
    stopAction: function() {
        if (this.kd) {
            this.doAction = false;
            this.kd = false;
        }
    }
    
});


Crafty.c("enemy", {
    _target: null,
    enemy: function() {
        var x, y;
        x = (player.x > ROOM_SIZE/2) ? Crafty.math.randomInt(0, player.x - 50) : Crafty.math.randomInt(player.x + 50, ROOM_SIZE-100);
        y = (player.y > ROOM_SIZE/2) ? Crafty.math.randomInt(0, player.y - 50) : Crafty.math.randomInt(player.y + 50, ROOM_SIZE-100);
        this.attr({x: x, y:y, w: 30, h: 30, z: 4})                
        
        return this;
    },
    init: function() {
        this.bind("EnterFrame", function() {
            var dx = (player.x - this.x) < 0 ? -1 : 1;
            var dy = (player.y - this.y) < 0 ? -1 : 1;
            this.x += dx;
            this.y += dy;
        });
        return this;
    }
});

Crafty.c("key", {
    _id: null,
    setObjId: function(id) {
        this._id = id;
        return this;
    }
});


Crafty.c("pocket", {
    content: null,
    show: function(o) {
        this.content.push(o);
        var y=this.y;
        for (var i=0; i<this.content.length; i++) {
            var obj = this.content[i];
            switch(obj.type) {
                case "key": 
                    obj = Crafty.e("2D, Canvas, Color")
                        .attr({x: this.x, y: y, w: obj.w, h: obj.h})
                        .color(obj.color);
                    y += obj.h + 5;
                    break;
            }
        }
    },
    pocket: function() {
        this.content = new Array();
        return this;
    }
});


var globalPocket;
var map = new MAP1();
var level = 0;
var separator=false;
var player=null;

Crafty.scene("Level1", function() {
    var room = Crafty.e("room, Image, 2D, Canvas")
                .attr({x: 0, y:0, w: 450, h: 450})
                .image('img/room.png');
       
       player = Crafty.e("hero, Image, 2D, Fourway, Canvas, Collision")
                .attr({x: 30, y:30, w: 30, h: 30, z: 5})
                .fourway(5)
                .collision()
                .image('img/hero2.png')
                .onHit('door', function(hitData) {
                    for (var i=0; i<hitData.length; i++) {
                        var hit = hitData[i];
                        var dir = hit.obj.exit;
                        map.move(dir);
                        for (var d=0; d<doors.length; ++d) {
                            doors[d].destroy();
                        }
                        //map.grid[map.gridPos].exit();
                        doors = changeRoom();
                        switch(dir) {
                            case 'W':
                                this.x = ROOM_SIZE-70;
                                break;
                            case 'E':
                                this.x = 30;
                                break;
                            case 'N':
                                this.y = ROOM_SIZE-70;
                                break;
                            case 'S':
                                this.y = 30;
                                break;
                        }
                    }
                
                })
                .onHit('key', function(hitData) {
                    for (var i=0; i<hitData.length; i++) {
                        var hit = hitData[i];
                        this.pick(map.pick(hit.obj._id));
                        doors.pop(hit.obj);
                        hit.obj.destroy();
                        Crafty.audio.play('pickup');
                    }
                })
                .onHit('gate', function(hitData) {
                    for (var i=0; i<hitData.length; i++) {
                        var hit = hitData[i];
                        if (hit.obj.open(this.pocket)) {
                            Crafty.audio.play('gate');
                            next_level();
                        }
                    }
                })
                .bind('KeyDown', function(e) {
                    if (e.key == Crafty.keys['SPACE']) {
                        var hitData = this.hit('switch');
                        if (hitData && this.canDoAction()) {
                            for (var i=0; i<hitData.length; i++) {
                                var hit = hitData[i];
                                map.rotateGrid(hit.obj.rotate);
                                hit.obj.toggle();
                                Crafty.audio.play('switch');
                            }
                            this.doAction = false;
                        }
                    }
                })
                .bind('KeyUp', function(e) {
                    if (e.key == Crafty.keys['SPACE']) {
                        this.stopAction();
                    }
                })
                .onHit('enemy', function(hitData) {
                    restartLevel();
                });
        
    
    console.log(player.pocket);
    var doors = changeRoom();
    globalPocket = Crafty.e("pocket, 2D")
                    .attr({x: ROOM_SIZE + 50, y:0, w: 100, h: 50})
                    .pocket();
    
                              
    
});

Crafty.scene("LevelChange", function() {
    var el;
    if (level === 0) {
        el = document.getElementById('instructions');
    } else {
        el = document.getElementById('next-level');
        document.getElementById('level-nr').textContent = (level+1).toString();
    }
    var get_ready = Crafty.e("2D, DOM, CustomHTML")
        .attr({ w: Crafty.viewport.width, h: Crafty.viewport.height})
        .customhtml(el)
        .bind("KeyDown", function(e) {
            if (e.key == Crafty.keys['SPACE']) {
                next_level();
            }
        });
});


Crafty.scene("TheEnd", function() {
    var el = document.getElementById('the-end');
    var get_ready = Crafty.e("2D, DOM, CustomHTML")
        .attr({ w: Crafty.viewport.width, h: Crafty.viewport.height})
        .customhtml(el);
});


function restartLevel() {
    level--;
    next_level();
}


function next_level() {
    if (separator) {
        level++;
        switch (level) {
            case 1:
                map = new MAP1();
                break;
            case 2:
                map = new MAP2();
                break;
            case 3:
                map = new MAP3();
                break;
        }
        Crafty.scene("Level1");
        separator = false;
    } else if (level < 3) {
        Crafty.scene("LevelChange");
        separator = true;
    } else {
        Crafty.scene("TheEnd");
        separator = true;
    }
        
}

var roomLabel = null;
function changeRoom() {
    var roomnr = map.grid[map.gridPos].currentRoom;
    var level=map.rooms[roomnr];
    if (DEBUG) {
        if (roomLabel) roomLabel.destroy();
        roomLabel = Crafty.e("2D, Text, Canvas").attr({ x: 100, y: 100 }).textColor('#FFFF00').text(roomnr.toString());
    }
    var doors = [];
    for (var i=0; i<level.doors.length; ++i) {
        var d = level.doors[i];
        doors.push(Crafty.e("door, 2D, Color, Canvas, Collision")
                .collision()
                .color('#000')
                .door(d.leads)
                  );
    }
    for (var i=0; i<level.objects.length; ++i) {
        var o = level.objects[i];
        var obj;
                        switch(o.type) {
                            case "key": 
                                obj = Crafty.e("key, Color, 2D, Canvas, Collision")
                                    .attr({x: o.x, y: o.y, w: o.w, h: o.h, z: 2})
                                    .collision()
                                    .color(o.color)
                                    .setObjId(o.id);
                                    break;
                            case "switch":
                                obj = Crafty.e("switch, Color, 2D, Canvas, Collision")
                                    .attr({x: o.x, y: o.y, w: o.w, h: o.h, z: 2})
                                    .collision()
                                    .swtx(o.rotate, o)
                                    break;
                        }
        doors.push(obj);
    }
    if (level.gate) {
        d = level.gate;
        if (d.color) {
            doors.push(Crafty.e("gate, 2D, Color, Canvas, Collision")
                .collision()
                .color(d.color)
                .gate(d.needs)
                  );
        } else {
            doors.push(Crafty.e("gate, 2D, Image, Canvas, Collision")
                .collision()
                .image(d.image)
                .gate(d.needs)
                  );
        }
    }
    var enemy = Crafty.e("enemy, Image, 2D, Canvas, Collision")
                .collision()
                .image('img/enemy.png')
                .enemy();
    doors.push(enemy);
    return doors;
}